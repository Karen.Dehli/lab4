package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    private IGrid nowGeneration;

    public BriansBrain(int rows, int columns){
		nowGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();

    }

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < nowGeneration.numRows(); row++) {
			for (int col = 0; col < nowGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					nowGeneration.set(row, col, CellState.ALIVE);
				} else {
					nowGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return nowGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return nowGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return nowGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = nowGeneration.copy();
		
	
		for (int i=0;i<numberOfRows();i++){
			for (int j=0;j<numberOfColumns();j++){
				

				nextGeneration.set(i,j,getNextCell(i, j));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState toReturn=CellState.ALIVE;
		CellState now=nowGeneration.get(row, col);

		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);


		
		if (now.equals(CellState.DEAD)&&(aliveNeighbors==3)){
				toReturn =CellState.ALIVE;
		}
		if (now.equals(CellState.ALIVE)){
			toReturn =CellState.DYING;
			}
			
		else if (now.equals(CellState.DYING)){
            toReturn=CellState.DEAD;
        }
        else{
            if (aliveNeighbors==2){
                toReturn=CellState.ALIVE;
            
            }
            else{toReturn=CellState.DEAD;}
        }

			
			
			return toReturn;

		}
		

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) throws IndexOutOfBoundsException {
		int countAlive=0;

		if (col-1<0||col+1>numberOfColumns()){
			throw new IndexOutOfBoundsException();}

		if (row-1<0||row+1>numberOfRows()){
				throw new IndexOutOfBoundsException();}
		for (int i=-1;i<2;i++){
			
			for (int j=-1;j<2;j++){
				
				CellState neighborState=nowGeneration.get(row+i, col+j);
				if (neighborState.equals(CellState.ALIVE)){
					countAlive++;
				}
			}
		}
		return countAlive;
	}

	@Override
	public IGrid getGrid() {
		return nowGeneration;
	}
}
