package cellular;

import java.awt.Color;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	private int rows;
	private int columns;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		this.rows=rows;
		this.columns=columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
	
		for (int i=0;i<numberOfRows();i++){
			for (int j=0;j<numberOfColumns();j++){
				

				nextGeneration.set(i,j,getNextCell(i, j));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState toReturn=CellState.ALIVE;
		CellState now=currentGeneration.get(row, col);

		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);


		
		if (now.equals(CellState.DEAD)&&(aliveNeighbors==3)){
				toReturn =CellState.ALIVE;
		}
		if (now.equals(CellState.ALIVE)){
			aliveNeighbors--;
			if (aliveNeighbors<2||aliveNeighbors>3){
				toReturn =CellState.DEAD;
			}
			if (aliveNeighbors==2||aliveNeighbors==3){
				toReturn=CellState.ALIVE;
			}

			}
			
			return toReturn;

		}
		

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) throws IndexOutOfBoundsException {
		int countAlive=0;

		if (col-1<0||col+1>numberOfColumns()){
			throw new IndexOutOfBoundsException();}

		if (row-1<0||row+1>numberOfRows()){
				throw new IndexOutOfBoundsException();}
		for (int i=-1;i<2;i++){
			
			for (int j=-1;j<2;j++){
				
				CellState neighborState=currentGeneration.get(row+i, col+j);
				if (neighborState.equals(CellState.ALIVE)){
					countAlive++;
				}
			}
		}
		return countAlive;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
