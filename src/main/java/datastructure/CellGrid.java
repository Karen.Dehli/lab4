package datastructure;

import cellular.CellState;
import cellular.GameOfLife;

public class CellGrid implements IGrid {
    int rows=200;
    int columns=200;
    CellState initialState;
    
    CellState[][] grid = new CellState[rows][columns];

    

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows=rows;
        this.columns=columns;
        this.initialState=initialState;
        for (int i=0;i<rows;i++){
            for (int j=0;j<columns;j++){
                grid[i][j]=initialState;
            }
        }
        


	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException{
        if (row<0||row>rows){
            throw new IndexOutOfBoundsException();
        }
        if (column<0||column>columns){
            throw new IndexOutOfBoundsException();
        }

        grid[row][column]=element;
        
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException{
        if (row<0||row>rows){
            throw new IndexOutOfBoundsException();
        }
        if (column<0||column>columns){
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
    
        IGrid copyGrid=new CellGrid(rows, columns, initialState);
        for (int i=0;i<rows;i++){
			for (int j=0;j<rows;j++){
				copyGrid.set(i,j,get(i,j));
			}}
        
        return copyGrid;
    }
    
}
